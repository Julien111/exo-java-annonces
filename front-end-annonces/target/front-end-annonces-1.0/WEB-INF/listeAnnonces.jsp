<%-- 
    Document   : listeAnnonces
    Created on : 1 févr. 2022, 22:02:46
    Author     : Julien
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Liste des annonces</title>
    </head>
    <body>
        <h1>Les annonces du site</h1>
        
        <c:forEach items="${annonces}" var="annonce">
            <div>
                <h2>${annonce.title}</h2>
                <p>${annonce.description}</p>
            </div>
            
        </c:forEach>
        
        <a href="http://localhost:8080/front-end-annonces">Retour accueil</a>
        
    </body>
</html>
