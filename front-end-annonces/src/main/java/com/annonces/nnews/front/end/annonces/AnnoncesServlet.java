/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.annonces.nnews.front.end.annonces;

import com.annonces.nnews.core.model.Annonces;
import com.annonces.nnews.core.model.Author;
import com.annonces.nnews.core.model.ListeAnnonces;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author J
 */
@WebServlet(name = "AnnoncesServlet", urlPatterns = {"/annonces"})
public class AnnoncesServlet extends HttpServlet {

    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        //on va créer les annonces
        if(ListeAnnonces.listeAnnonces.isEmpty()){
            
            Annonces annonceOne = new Annonces("L'économie mondiale");
            Annonces annonceTwo = new Annonces("Le football business");
            Annonces annonceThree = new Annonces("The AEW in USA");
            
            Author tom = new Author("Tom Davis");
            Author mike = new Author("Mike David");
            
            annonceOne.setDescription("Nec piget dicere avide magis hanc insulam populum Romanum invasisse quam iuste. Ptolomaeo enim rege foederato nobis et socio ob aerarii nostri angustias iusso sine ulla culpa proscribi ideoque hausto veneno.");
            annonceTwo.setDescription("Nec piget dicere avide magis hanc insulam populum Romanum invasisse quam iuste. Ptolomaeo enim rege foederato nobis et socio ob aerarii nostri angustias iusso sine ulla culpa proscribi ideoque hausto veneno.");
            annonceThree.setDescription("Nec piget dicere avide magis hanc insulam populum Romanum invasisse quam iuste. Ptolomaeo enim rege foederato nobis et socio ob aerarii nostri angustias iusso sine ulla culpa proscribi ideoque hausto veneno.");
            
            //ajout de l'auteur
            annonceOne.setTheAuthor(tom);
            annonceTwo.setTheAuthor(tom);
            annonceThree.setTheAuthor(mike);
            
            ListeAnnonces.listeAnnonces.add(annonceOne);
            ListeAnnonces.listeAnnonces.add(annonceTwo);
            ListeAnnonces.listeAnnonces.add(annonceThree);
        }
        
        request.setAttribute("annonces", ListeAnnonces.listeAnnonces);
        RequestDispatcher disp =  request.getRequestDispatcher("/WEB-INF/listeAnnonces.jsp");
        disp.forward(request, response);
        
    }
    

}
