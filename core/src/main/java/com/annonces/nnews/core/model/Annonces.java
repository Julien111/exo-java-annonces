/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.annonces.nnews.core.model;



/**
 *
 * @author J
 */
public class Annonces {
    
    private static int lastId;
    
    private long id;
    
    private String title;
    
    private String description;
    
    private Author theAuthor;
    
    public Annonces() {
        this.id = lastId++;
    }

    public Annonces(String title) {
        this();
        this.title = title;
    }   
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }    

    public Author getTheAuthor() {
        return theAuthor;
    }

    public void setTheAuthor(Author theAuthor) {
        this.theAuthor = theAuthor;
    }           
    
}
